Atmel AVR C system: Status Pusher
=================================

About
=====

<https://gitlab.com/marsin/myAVR-MK3_c-system_status-pusher>

* This project is a kind debugger system, using the TWI interface for receiving,
  the LCD for displaying and the UART for pushing status codes.
* This system is made for the myAVR-MK3 (ATmega2560) development board.

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Requirements
------------

### General

* avr-gcc
* avrdude


### Libraries

The MK3 library is required for the test functions,
but not for compiling and installing this TWI library.

* <https://gitlab.com/marsin/myAVR-MK3_c-driver_libmk3>
* <https://gitlab.com/marsin/myAVR-MK3_c-driver_libtwi>
* <https://gitlab.com/marsin/myAVR-MK3_c-driver_libuart>

* myAVR MK3 board:
  - en: <http://shop.myavr.com/?sp=article.sp.php&artID=100063>
  - de: <http://shop.myavr.de/?sp=article.sp.php&artID=100063>


Documentation
-------------

Creating the Doxygen reference:

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html


Usage
-----

Edit the Makefile!

* `make all`        Make software.
* `make clean`      Clean out built project files.
* `make program`    Download the hex file to the device, using avrdude.


### Programming the MC (MK3 board)

Select a test function with the compiler directive TEST,
defined in the Makefile or by the make command.

	$ make clean && make TEST=1 && make install


Usage Hints
-----------

### Configuring the serial device on PC with `udev`

This configuration is useful to find the device always under the same name.

In general the device appears under `/dev/ttyUSBx`.
Depending if there are other serial USB devices and when the device is connected
devices can change their number (the 'x' in `/dev/ttyUSBx`).
With an udev configuration a device can get recognized by different parameter
like Vendor ID and linked to an individual device name (like `/dev/mk3uart`).


#### Recipe

1. Connect the UART-Bridge USB cable with the PC
2. Get information about the serial device:

	# lsusb
	# lsusb -v -s <bus_id>:<device_id>
	
	# udevadm info -a -p $(udevadm info -q path -n /dev/<device_name>)


3. Create a udev rule (e.g. `/etc/udev/rules.d/50-mars.rules`):

Edit the parameter fitting to your device.

	# MyAVR - MK3 development board UART3
	KERNEL=="ttyUSB[0-9]", SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", ATTRS{product}=="CP2102 USB to UART Bridge Controller", SYMLINK+="mk3uart"


4. Apply the settings

	# udevadm control --reload

or

	# udevadm trigger

5. Disconnect and Reconnect UART device


### Connect the UART

	# minicom --device /dev/mk3uart --baudrate 9600 --wrap
	# minicom --device /dev/mk3uart --baudrate 9600 --wrap --displayhex


System Functions
================

...


Test Functions
==============

Not all test functions are implemented yet!

* src/test.h
* src/test.c

